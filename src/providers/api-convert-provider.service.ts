import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import 'rxjs-compat/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ApiConvertProviderService {

  url;
  service : string;
  mode;
  ccy;

  constructor(public http: HttpClient) {
    // this.url = 'http://converter-back.loc';
    this.url = 'https://api.web-world.com.ua';
  }

  getCurrencyRatesAvailable(service: string) {
    let httpOptions = {
      headers: new HttpHeaders()
        .set('userName', 'api-test-user')
        .set('userPassword', 'api-test-password'),

      params: new HttpParams()
        .set('service', service || 'pr')
        .set('ccy', this.ccy || 'UAH')
        .set('mode', 'ccy-available')
    };

    return this.http.get(this.url, httpOptions)
      .map(res => res);
  }

  getCurrencyRates(ccy: string, service: string) {
    let httpOptions = {
      headers: new HttpHeaders()
        .set('userName', 'api-test-user')
        .set('userPassword', 'api-test-password'),

      params: new HttpParams()
        .set('service', service || 'pr')
        .set('ccy', ccy || 'UAH')
    };

    return this.http.get(this.url, httpOptions)
      .map(res => res);
  }
}
