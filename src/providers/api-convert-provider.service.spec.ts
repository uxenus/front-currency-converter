import { TestBed, inject } from '@angular/core/testing';

import { ApiConvertProviderService } from './api-convert-provider.service';

describe('ApiConvertProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiConvertProviderService]
    });
  });

  it('should be created', inject([ApiConvertProviderService], (service: ApiConvertProviderService) => {
    expect(service).toBeTruthy();
  }));
});
