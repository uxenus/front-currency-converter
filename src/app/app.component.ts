import {Component, OnInit} from '@angular/core';
import {ApiConvertProviderService} from '../providers/api-convert-provider.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Добро пожаловать!';

  selectCCY: string;
  dollarAmount: number;
  RATE: number;

  resultConvertData: any;

  convertService: string;

  allCurr;
  rawData : any;
  // dateForApi = new Date(Date.now()).toLocaleString().split(',')[0];

  constructor(
    private apiProvider: ApiConvertProviderService) {
  }

  ngOnInit(): void {
    this.selectCCY = this.selectCCY || 'UAH';
    this.dollarAmount = this.dollarAmount || 1;
    this.convertService = this.convertService || 'pr';

    this.apiProvider
      .getCurrencyRatesAvailable(this.convertService)
      .subscribe(resp => {
        if (resp !== null) {
          this.rawData = resp;
          this.allCurr = this.rawData.result;

        } else {
          this.allCurr = {};
        }
      });
  }

  goConvert () {
    this.resultConvertData = false;

    if(!this.selectCCY || !this.convertService) {
      return false;
    }

    this.apiProvider
      .getCurrencyRates(this.selectCCY, this.convertService)
      .subscribe(resp => {
        if (resp !== null) {
          this.rawData = resp;
          this.RATE = this.rawData.result;

          this.resultConvertData = this.RATE*this.dollarAmount;

        } else {
          this.RATE = 1;
        }
      });
  }

  changeService(event) {

    this.resultConvertData = false;
    this.selectCCY = '';

    this.apiProvider
      .getCurrencyRatesAvailable(event.target.value)
      .subscribe(resp => {
        if (resp !== null) {
          this.rawData = resp;
          this.allCurr = this.rawData.result;
        } else {
          this.allCurr = {};
        }
      });
  }

}
